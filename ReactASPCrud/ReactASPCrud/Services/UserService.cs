using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ReactASPCrud.Exceptions;
using ReactASPCrud.Models;
using ReactASPCrud.Persistance;
using ReactASPCrud.Utils;

namespace ReactASPCrud.Services
{
    public class UserService
    {
        private readonly ReactASPCrudContext _reactASPCrudContext;

        public UserService(ReactASPCrudContext reactASPCrudContext)
        {
            _reactASPCrudContext = reactASPCrudContext;
        }

        public async Task<List<User>> GetAll()
        {
            return await _reactASPCrudContext.User
                .OrderBy(u => u.Id)
                .Select(u => u.FromUserEntity()).ToListAsync();
        }

        public async Task<User> GetById(int id)
        {
            var user = await GetUserEntityById(id);
            return user.FromUserEntity();
            // return users.Where(user => user.Id == id).FirstOrDefault();
        }

        public async Task<User> Create(User user)
        {
            var userExists = _reactASPCrudContext.User.Any(u => u.Email == user.Email);
            if (userExists)
            {
                throw new DuplicatedUserException($"User with email {user.Email} already exists");
            }
            byte[] salt = Password.GenerateSalt();
            var password = Password.HashPassword(user.Password, salt);

            var userEnt = new ReactASPCrud.Persistance.Entities.User
            {
                Name = user.Name,
                Document = user.Document,
                Email = user.Email,
                Phone = user.Phone,
                Password = password,
                Salt = Password.SaltAsString(salt)
            };

            await _reactASPCrudContext.AddAsync(userEnt);
            await _reactASPCrudContext.SaveChangesAsync();

            user.Id = userEnt.Id;
            user.Password = string.Empty;

            return user;
        }

        public async Task Update(int id, User user)
        {
            try
            {
                var found = await GetUserEntityById(id);
                found.Name = user.Name;
                found.Email = user.Email;
                found.Document = user.Document;
                found.Phone = user.Phone;

                if (!string.IsNullOrWhiteSpace(user.Password))
                {
                    var password = Password.HashPassword(user.Password, Password.SaltFromString(found.Salt));
                    found.Password = password;
                }

                await _reactASPCrudContext.SaveChangesAsync();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task Delete(int id)
        {
            var user = await GetUserEntityById(id);
            _reactASPCrudContext.User.Remove(user);
            await _reactASPCrudContext.SaveChangesAsync();
        }

        private async Task<ReactASPCrud.Persistance.Entities.User> GetUserEntityById(int id)
        {
            return await _reactASPCrudContext.User.SingleAsync(u => u.Id == id);
        }
    }
}
