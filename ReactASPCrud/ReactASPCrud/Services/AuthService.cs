﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using ReactASPCrud.Exceptions;
using ReactASPCrud.Models;
using ReactASPCrud.Persistance;
using ReactASPCrud.Utils;

namespace ReactASPCrud.Services
{
    public class AuthService
    {
        private readonly ReactASPCrudContext _reactASPCrudContext;
        private readonly AppSettings _appSettings;

        public AuthService(ReactASPCrudContext reactASPCrudContext, IOptions<AppSettings> appSettings)
        {
            _reactASPCrudContext = reactASPCrudContext;
            _appSettings = appSettings.Value;
        }

        public async Task<UserAuth> LoginAsync(string email, string password)
        {
            try
            {
                var user = await _reactASPCrudContext.User.SingleAsync(u => u.Email == email);

                if (user.Password != Password.HashPassword(password, Password.SaltFromString(user.Salt)))
                {
                    // throw error
                    throw new InvalidLoginAttemptException("Login failed");
                }

                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[] {
                        new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                        new Claim(ClaimTypes.Name, user.Name),
                        new Claim(ClaimTypes.Email, user.Email),
                    }),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };
                var token = tokenHandler.CreateToken(tokenDescriptor);

                return new UserAuth
                {
                    Token = tokenHandler.WriteToken(token),
                    Email = user.Email,
                    Id = user.Id,
                    Name = user.Name,
                };
            }
            // No user was found
            catch (InvalidOperationException ex)
            {
                throw new InvalidLoginAttemptException("Login failed", ex);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
