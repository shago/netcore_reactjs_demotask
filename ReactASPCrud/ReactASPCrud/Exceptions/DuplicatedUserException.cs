﻿using System;

namespace ReactASPCrud.Exceptions
{
    public class DuplicatedUserException : Exception
    {
        public DuplicatedUserException() : base()
        {
        }

        public DuplicatedUserException(string message) : base(message)
        {
        }

        public DuplicatedUserException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
