﻿using ReactASPCrud.Models;

namespace ReactASPCrud.Utils
{
    public static class UserExtension
    {
        public static User FromUserEntity(this ReactASPCrud.Persistance.Entities.User usr)
        {
            return new User
            {
                Id = usr.Id,
                Document = usr.Document,
                Email = usr.Email,
                Name = usr.Name,
                Phone = usr.Phone,
            };
        }
    }
}
