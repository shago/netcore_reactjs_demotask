﻿
namespace ReactASPCrud.Persistance.Entities
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Document { get; set; }
        public string Phone { get; set; }

        public string Password { get; set; }
        public string Salt { get; set; }
    }
}
