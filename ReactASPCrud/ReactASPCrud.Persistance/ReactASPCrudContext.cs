﻿using Microsoft.EntityFrameworkCore;
using ReactASPCrud.Persistance.Entities;

namespace ReactASPCrud.Persistance
{
    public class ReactASPCrudContext : DbContext
    {
        public ReactASPCrudContext(DbContextOptions<ReactASPCrudContext> options) : base(options)
        {
        }

        public DbSet<User> User { get; set; }
    }
}
