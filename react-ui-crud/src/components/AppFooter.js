import React, { Fragment } from 'react';

class AppFooter extends React.Component {

    render() {
        return <Fragment>
            <hr className="featurette-divider" />

            <footer style={{ padding: "0 50px" }} className="navbar fixed-bottom">
                <p className="float-right"></p>
                <p>© 2021 <a target="_blank" href="http://www.kambda.com">Kambda</a></p>
            </footer>
        </Fragment>;
    }
}

export default AppFooter;