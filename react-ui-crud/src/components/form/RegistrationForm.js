import React from 'react';
import { Button, Form, FormGroup, Input, Label, Alert } from 'reactstrap';

import { USERS_API_URL } from '../../constants';

class RegistrationForm extends React.Component {

    state = {
        id: 0,
        name: '',
        document: '',
        email: '',
        phone: '',
        password: '',
        errMessage: ''
    }

    componentDidMount() {
        if (this.props.user) {
            const { id, name, document, email, phone } = this.props.user
            this.setState({ id, name, document, email, phone });
        }
    }

    onChange = e => {
        this.setState({ [e.target.name]: e.target.value })
    }

    submitNew = async e => {
        e.preventDefault();
        const res = await fetch(`${USERS_API_URL}`, {
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name: this.state.name,
                document: this.state.document,
                email: this.state.email,
                phone: this.state.phone,
                password: this.state.password
            })
        });

        if (!res.ok) {
            await this.handleError(res);
        }
        else {
            const user = await res.json();
            this.props.addUserToState(user);
            this.props.toggle();
        }
    }

    handleError = errRes => {
        return errRes.json()
            .then(res => {
                this.setState({ errMessage: res });
            });
    }

    submitEdit = async e => {
        e.preventDefault();
        try {
            const res = await fetch(`${USERS_API_URL}/${this.state.id}`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem('token'),
                },
                body: JSON.stringify({
                    name: this.state.name,
                    document: this.state.document,
                    email: this.state.email,
                    phone: this.state.phone,
                    password: this.state.password
                })
            });
            this.props.toggle();
            this.props.updateUserIntoState(this.state.id);
        }
        catch (err) {
            console.error(err)
        }
    }

    render() {
        return <Form onSubmit={this.props.user ? this.submitEdit : this.submitNew}>
            <FormGroup>
                <Label for="name">Name:</Label>
                <Input type="text" name="name" onChange={this.onChange} value={this.state.name === '' ? '' : this.state.name} />
            </FormGroup>
            <FormGroup>
                <Label for="document">Document:</Label>
                <Input type="text" name="document" onChange={this.onChange} value={this.state.document === null ? '' : this.state.document} />
            </FormGroup>
            <FormGroup>
                <Label for="email">Email:</Label>
                <Input type="email" name="email" onChange={this.onChange} value={this.state.email === null ? '' : this.state.email} />
            </FormGroup>
            <FormGroup>
                <Label for="phone">Phone:</Label>
                <Input type="text" name="phone" onChange={this.onChange} value={this.state.phone === null ? '' : this.state.phone}
                    placeholder="+1 999-999-9999" />
            </FormGroup>
            <FormGroup>
                <Label for="password">Password:</Label>
                <Input type="password" name="password" onChange={this.onChange} value={this.state.password === null ? '' : this.state.password} />
            </FormGroup>
            {this.state.errMessage &&
                <Alert color="danger">
                    {this.state.errMessage}
                </Alert>
            }
            <Button>Send</Button>
        </Form>;
    }
}

export default RegistrationForm;