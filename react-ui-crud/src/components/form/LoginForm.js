import React from 'react';
import { Button, Form, FormGroup, Input, Label, Alert, Container, Row, Col } from 'reactstrap';

import RegistrationModal from './RegistrationModal';

import { AUTH_API_URL } from '../../constants';

class LoginForm extends React.Component {

    state = {
        email: '',
        password: '',
        errMessage: '',
    }

    onChange = e => {
        this.setState({ [e.target.name]: e.target.value })
    }

    login = async e => {
        e.preventDefault();
        try {
            const res = await fetch(`${AUTH_API_URL}/login`, {
                method: 'post',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    email: this.state.email,
                    password: this.state.password,
                })
            });

            if (!res.ok) {
                await this.handleError(res);
            } else {
                const user = await res.json();
                localStorage.setItem('token', user.token);
                this.props.onLogInStateChanged(true);
            }
        } catch (err) {
            this.setState({ errMessage: err.message });
        }

    }

    handleError = errRes => {
        return errRes.json()
            .then(res => {
                this.setState({ errMessage: res });
            });
    }

    render() {
        return (
            <Container style={{ paddingTop: "100px" }}>
                <Row>
                    <Col>
                        <Form onSubmit={this.login}>
                            <FormGroup>
                                <Label for="email">Email:</Label>
                                <Input type="email" name="email" onChange={this.onChange} value={this.state.email === null ? '' : this.state.email} />
                            </FormGroup>
                            <FormGroup>
                                <Label for="password">Password:</Label>
                                <Input type="password" name="password" onChange={this.onChange} value={this.state.password === null ? '' : this.state.password} />
                            </FormGroup>

                            {this.state.errMessage &&
                                <Alert color="danger">
                                    {this.state.errMessage}
                                </Alert>
                            }
                            <Button style={{ minWidth: "200px" }}>Login</Button>
                        </Form>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <RegistrationModal isNew={true} title='Register' addUserToState={() => { }} />
                    </Col>
                </Row>

            </Container>);
    }
}

export default LoginForm;