import React, { Component, Fragment } from 'react';
import AppHeader from './components/AppHeader';
import Home from './components/Home';
import AppFooter from './components/AppFooter';
import LoginForm from './components/form/LoginForm';

class App extends Component {

  state = {
    isLoggedIn: false
  }

  componentDidMount() {
    const token = localStorage.getItem('token');
    if (token) {
      this.setState({ isLoggedIn: true });
    }
  }

  onLogInStateChanged = (logedIn) => {
    this.setState({ isLoggedIn: logedIn })
  }

  render() {
    return <Fragment>
      <AppHeader isLoggedIn={this.state.isLoggedIn} onLogInStateChanged={this.onLogInStateChanged} />
      {this.state.isLoggedIn &&
        <Home />
      }
      {!this.state.isLoggedIn
        && <LoginForm onLogInStateChanged={this.onLogInStateChanged} />}

      <AppFooter />
    </Fragment>;
  }
}

export default App;
